import { Ai } from './vendor/@cloudflare/ai';

export default {
  async fetch(request, env) {
    const tg_bot_token = "" // Your TG Bot Token Here
    const maxMessageLength = 4096;
    var url = new URL(request.url);
    var path = url.pathname;
    if (path == '/' && request.method === 'POST') {
      var data = JSON.stringify(await request.json());
      console.log(data)
      var obj = JSON.parse(data);
      try {
        if (obj.hasOwnProperty('message')) {
          if (obj.message.hasOwnProperty('text')) {
            const command = obj.message.text
            const tasks = [];
            const ai = new Ai(env.AI);
            let chat = {
              messages: [
                { role: 'system', content: 'You are a helpful assistant.' },
                { role: 'user', content: command }
              ]
            };
            let response = await ai.run('@cf/meta/llama-2-7b-chat-int8', chat);
            
            tasks.push({ inputs: chat, response });
            
            // Extract only the final answer from the response
            const conversation = response.response; // Split the response by newline
            /*const finalAnswer = conversation.filter(line => line.startsWith('Answer:'))[0]; // Find and extract the final answer
            if (finalAnswer ==  undefined) {
              await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id="+obj.message.chat.id+"&reply_to_message_id="+obj.message.message_id+"&text=Something went wrong.\n\n"+conversation, {
                method: "GET"
              });
            }*/
            const messages = [];
            let currentMessage = '';
            
            for (let i = 0; i < conversation.length; i += maxMessageLength) {
              const part = conversation.substr(i, maxMessageLength);
              messages.push(part);
            }
            // Send each message
            for (const message of messages) {
              console.log("Sending "+ message)
              await fetch(`https://api.telegram.org/bot${tg_bot_token}/sendMessage`, {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                  chat_id: obj.message.chat.id,
                  reply_to_message_id: obj.message.message_id,
                  text: message,
                  disable_web_page_preview: true,
                }),
              });
            }
            return new Response(conversation, {
              status: 200,
              headers: {
                  "content-type": "application/json",
              },
            })
            /*return new Response(finalAnswer, {
              headers: { 'Content-Type': 'text/plain' }, // Set the content type to plain text
            });*/  
          }
        } 
        return new Response("OK", {
          status: 200,
          headers: {
              "content-type": "application/json",
          },
        })
      } catch (error) {
        await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id="+obj.message.chat.id+"&reply_to_message_id="+obj.message.message_id+"&text=Something went wrong.\n\n"+error, {
          method: "GET"
        });
        return new Response("OK", {
          status: 200,
          headers: {
              "content-type": "application/json",
          },
        })
      }
      await fetch("https://api.telegram.org/bot" + tg_bot_token + "/SendMessage?disable_web_page_preview=true&chat_id="+obj.message.chat.id+"&reply_to_message_id="+obj.message.message_id+"&text=Something went wrong.", {
        method: "GET"
      });
      return new Response("OK", {
        status: 200,
        headers: {
            "content-type": "application/json",
        },
      })
    }
    return new Response("OK", {
      status: 200,
      headers: {
          "content-type": "application/json",
      },
    })
    // messages - chat style input
    /*let chat = {
      messages: [
        { role: 'system', content: 'You are a helpful assistant.' },
        { role: 'user', content: 'Who won the world series in 2020?' }
      ]
    };
    response = await ai.run('@cf/meta/llama-2-7b-chat-int8', chat);
    tasks.push({ inputs: chat, response });*/

    // prompt - simple completion style input 
  }
};
